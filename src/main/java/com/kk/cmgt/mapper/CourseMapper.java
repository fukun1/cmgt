package com.kk.cmgt.mapper;

import com.kk.cmgt.pojo.Booking;
import com.kk.cmgt.pojo.Classs;
import com.kk.cmgt.pojo.Course;
import org.apache.ibatis.annotations.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Mapper
@Transactional
public interface CourseMapper {

    @Select("select * from course")
    @Results(id="courseMap",value = {
            @Result(id=true,column = "cID",property = "courseID"),
            @Result(column = "cName",property = "courseName"),
            @Result(column = "cTeacher",property = "courseTeacher"),
            @Result(column = "cClass",property = "courseClass"),
            @Result(column = "cWeek",property = "courseWeek"),
            @Result(column = "cDay",property = "courseDay"),
            @Result(column = "cTimer",property = "courseTimer"),
            @Result(column = "status",property = "status"),
            @Result(column = "crID",property = "classRoomID"),
            @Result(column = "cCreateTime",property = "courseCreateTime"),
            @Result(column = "cUpdateTime",property = "courseUpdateTime")
    })
    List<Course> selectAllCourse();

    @Select("select * from course where cID=#{id}")
    @ResultMap(value = {"courseMap"})
    Course selectCourseById(int id);

    @Insert("insert into course(" +
            "cID,cName,cPeopleNumber,cDepartment,cGrade,cProfession,cCreateTime,cUpdateTime) " +
            "values(#{classId},#{className},#{classPeopleNumber},#{classDepartment},#{classGrade},#{classProfession},#{classCreateTime},#{classUpdateTime})")
    int insertCourse(Course course);


    @Delete("delete from course where cID=#{id}")
    int deleteCourse(int id);
}
