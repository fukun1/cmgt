package com.kk.cmgt.mapper;

import com.kk.cmgt.pojo.Booking;
import com.kk.cmgt.pojo.TeachingBuild;
import com.kk.cmgt.pojo.User;
import org.apache.ibatis.annotations.*;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Mapper
@Transactional
public interface UserMapper {

    @Select("select * from user")
    @Results(id="userMap",value = {
            @Result(id=true,column = "uID",property = "userID"),
            @Result(column = "rID",property = "classID"),
            @Result(column = "uStudentID",property = "userStudentID"),
            @Result(column = "uPassword",property = "userPassword"),
            @Result(column = "uName",property = "userName"),
            @Result(column = "uGender",property = "userGender"),
            @Result(column = "uNation",property = "userNation"),
            @Result(column = "uBirthday",property = "userBirthday"),
            @Result(column = "uCreateTime",property = "userCreateTime"),
            @Result(column = "uUpdateTime",property = "userUpdateTime"),
            @Result(column = "uStatus",property = "userStatus")
    })
    List<User> selectAllUser();

    @Select("select * from user where uStudentID=#{studentId}")
    @ResultMap(value = {"userMap"})
    User selectUserByStudentId(int studentId);

    @Select("select * from user where uID=#{userId}")
    @ResultMap(value = {"userMap"})
    User selectUserById(int userId);

    @Select("select * from user where uName=#{username}")
    @ResultMap(value = {"userMap"})
    User selectUserByName(String username);

    @Delete("delete from user where uID=#{userId}")
    int deleteUser(int userId);

    @Insert("insert into user values(#{userID},#{classID},#{userStudentID},#{userPassword},#{userName},#{userGender},#{userNation},#{userBirthday},#{userCreateTime},#{userUpdateTime},#{userStatus})")
    //@ResultMap(value = {"userMap"})
    int insertUser(User user);

    @Update("update user set " +
            "uName=#{userName}," +
            "uPassword=#{userPassword}," +
            "uUpdateTime=#{userUpdateTime} " +
            "where uID=#{userID}")
    int updateUser(User user);

    @Update("update user set " +
            "uName=#{userName}," +
            "uGender=#{userGender}," +
            "uStudentID=#{userStudentID}," +
            "uUpdateTime=#{userUpdateTime}," +
            "uNation=#{userNation} " +
            "where uID=#{userID}")
    int updateUserInformation(User user);

    @Update("update user set " +
            "uPassword=#{userPassword}" +
            "where uID=#{userID}")
    int updateUserPassword(User user);
}
