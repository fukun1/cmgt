package com.kk.cmgt.mapper;

import com.kk.cmgt.pojo.TeachingBuild;
import lombok.Data;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Mapper
@Transactional
public interface TeachingBuildMapper {

    @Select("select * from teachingbuild")
    @Results(id="teachingbuildMap",value = {
            @Result(id=true,column = "tID",property = "teachingBuildID"),
            @Result(column = "tName",property = "teachingBuildName"),
            @Result(column = "tFloors",property = "teachingBuildFloors"),
            @Result(column = "tCreateTime",property = "teachingBuildCreateTime"),
            @Result(column = "tUpdateTime",property = "teachingBuildUpdateTime")
    })
    List<TeachingBuild> selectAllTeachingBuild();

    @Select("select * from teachingbuild where tID=#{teachingBuildID}")
    @ResultMap(value = {"teachingbuildMap"})
    TeachingBuild selectTeachingBuildById(int teachingBuildID);

    @Delete("delete from teachingbuild where tID=#{teachingBuildID}")
    int deleteTeachingBuild(int teachingBuildID);

    @Insert("insert into teachingbuild values(#{teachingBuildID},#{teachingBuildName},#{teachingBuildFloors},#{teachingBuildCreateTime},#{teachingBuildUpdateTime})")
    int InsertTeachingBuild(TeachingBuild teachingBuild);

}
