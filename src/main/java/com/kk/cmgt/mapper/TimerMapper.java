package com.kk.cmgt.mapper;

import com.kk.cmgt.pojo.TeachingBuild;
import com.kk.cmgt.pojo.Timer;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Mapper
@Transactional
public interface TimerMapper {

    @Select("select * from timer")
    @Results(id="timerMap",value = {
            @Result(id=true,column = "timerId",property = "timerId"),
            @Result(column = "timerTime",property = "timerTime")
    })
    List<Timer> selectAllTimer();

    @Select("select * from timer where timerId=#{timerId}")
    @ResultMap(value = {"timerMap"})
    Timer selectTimerById(int timerId);




}
