package com.kk.cmgt.mapper;

import com.kk.cmgt.pojo.Booking;
import com.kk.cmgt.pojo.Classroom;
import com.kk.cmgt.pojo.Classs;
import org.apache.ibatis.annotations.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Mapper
@Transactional
public interface ClassroomMapper {

    @Select("select * from classroom")
    @Results(id="classroomMap",value = {
            @Result(id=true,column = "cID",property = "classroomId"),
            @Result(column = "cFloor",property = "classroomFloor"),
            @Result(column = "cCapacity",property = "classroomCapacity"),
            @Result(column = "cNumbering",property = "classroomNumbering"),
            @Result(column = "cType",property = "classroomType"),
            @Result(column = "cCreateTime",property = "classroomCreateTime"),
            @Result(column = "cUpdateTime",property = "classroomUpdateTime"),
            @Result(column = "cName",property = "classroomName"),
    })
    List<Classroom> selectAllClassroom();

    @Select("select * from classroom where cID=#{id}")
    @ResultMap(value = {"classroomMap"})
    Classroom selectClassroomById(int id);

    @Insert("insert into classroom(" +
            "cID,cFloor,cCapacity,cNumbering,cType,cCreateTime,cUpdateTime) " +
            "values(#{classroomId},#{classroomFloor},#{classroomCapacity},#{classroomNumbering},#{classroomType},#{classroomCreateTime},#{classroomUpdateTime},#{classroomName})")
    int insertClassroom(Classroom classroom);


    @Delete("delete from classroom where cID=#{id}")
    int deleteClassroom(int id);
}
