package com.kk.cmgt.mapper;

import com.kk.cmgt.pojo.Classs;
import com.kk.cmgt.pojo.StartTime;
import org.apache.ibatis.annotations.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Mapper
@Transactional
public interface StartTimeMapper {

    @Select("select * from starttime")
    @Results(id="startTimeMap",value = {
            @Result(id=true,column = "startTimeId",property = "startTimeId"),
            @Result(column = "startTime",property = "startTime")
    })
    List<StartTime> selectAllStartTime();

    @Select("select * from starttime where startTimeId=#{id}")
    @ResultMap(value = {"startTimeMap"})
    StartTime selectStartTimeById(int id);

    @Insert("insert into starttime values(#{startTimeId},#{startTime})")
    int insertStartTime(StartTime starttime);


    @Delete("delete from starttime where startTimeId=#{id}")
    int deleteStartTime(int id);

}
