package com.kk.cmgt.mapper;

import com.kk.cmgt.pojo.Booking;
import com.kk.cmgt.pojo.Classs;
import org.apache.ibatis.annotations.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Mapper
@Transactional
public interface ClasssMapper {

    @Select("select * from class")
    @Results(id="classMap",value = {
            @Result(id=true,column = "cID",property = "classId"),
            @Result(column = "cName",property = "className"),
            @Result(column = "cPeopleNumber",property = "classPeopleNumber"),
            @Result(column = "cDepartment",property = "classDepartment"),
            @Result(column = "cGrade",property = "classGrade"),
            @Result(column = "cProfession",property = "classProfession"),
            @Result(column = "cCreateTime",property = "classCreateTime"),
            @Result(column = "cUpdateTime",property = "classUpdateTime")
    })
    List<Classs> selectAllClass();

    @Select("select * from class where cID=#{id}")
    @ResultMap(value = {"classMap"})
    Classs selectClassById(int id);

    @Insert("insert into class(" +
            "cID,cName,cPeopleNumber,cDepartment,cGrade,cProfession,cCreateTime,cUpdateTime) " +
            "values(#{classId},#{className},#{classPeopleNumber},#{classDepartment},#{classGrade},#{classProfession},#{classCreateTime},#{classUpdateTime})")
    int insertClass(Classs classs);


    @Delete("delete from class where cID=#{id}")
    int deleteClass(int id);

}
