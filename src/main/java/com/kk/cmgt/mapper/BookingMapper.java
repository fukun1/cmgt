package com.kk.cmgt.mapper;

import com.kk.cmgt.pojo.Booking;
import org.apache.ibatis.annotations.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Mapper
@Transactional
public interface BookingMapper {

    @Select("select * from booking")
    @Results(id="bookingMap",value = {
            @Result(id=true,column = "bID",property = "bookingId"),
            @Result(column = "crID",property = "classroomId"),
            @Result(column = "cID",property = "classId"),
            @Result(column = "bContent",property = "bookingContent"),
            @Result(column = "bWeek",property = "bookingWeek"),
            @Result(column = "bWeekDay",property = "bookingWeekDay"),
            @Result(column = "bClass",property = "bookingClass"),
            @Result(column = "bCreateTime",property = "bookingCreateTime"),
            @Result(column = "bUpdateTime",property = "bookingUpdateTime")
    })
    List<Booking> selectAllBookings();

    @Select("select * from booking where bID=#{id}")
    @ResultMap(value = {"bookingMap"})
    Booking selectBookingById(int id);

    @Select("select * from booking where cID=#{classId}")
    @ResultMap(value = {"bookingMap"})
    List<Booking> selectBookingByClass(int classId);

    @Insert("insert into booking(" +
            "bID,crID,cID,bContent,bWeek,bWeekDay,bClass,bCreateTime,bUpdateTime) " +
            "values(#{bookingId},#{classroomId},#{classId},#{bookingContent},#{bookingWeek},#{bookingWeekDay},#{bookingClass},#{bookingCreateTime},#{bookingUpdateTime})")
    int insertBooking(Booking booking);

    @Update("update booking set " +
            "crID=#{classroomId}," +
            "cID=#{classId}," +
            "bContent=#{bookingContent}," +
            "bWeek=#{bookingWeek}," +
            "bWeekDay=#{bookingWeekDay}," +
            "bClass=#{bookingClass}," +
            "bCreateTime=#{bookingCreateTime}," +
            "bUpdateTime=#{bookingUpdateTime} where bID=#{bookingId}")
    int updateBooking(Booking booking);

    @Update("update booking set " +
            "bUpdateTime=#{bookingUpdateTime} where bID=#{bookingId}")
    int updateBookingUpdateTime(Booking booking);

    @Delete("delete from booking where bID=#{id}")
    int deleteBooking(int id);

}
