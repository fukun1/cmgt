package com.kk.cmgt.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseVo {
    private String teachingBuild;
    private String classroom;
    private String classroomType;
    private String things;
    private int courseVoStatus;//用于区分是课程占用还是借出 0为课程，1为借出
    private Date needTime;//占用日期
    private String weekDay;//星期
    private String time;//时间
}
