package com.kk.cmgt.vo;

import com.kk.cmgt.pojo.Booking;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.print.Book;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookingAdminVo extends Booking {

    private String  classRoomName;
    private int  classroomCapacity;

}
