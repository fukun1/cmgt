package com.kk.cmgt.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookingVo {
    private String teachingBuild;
    private String classroom;
    private String classroomType;
    private Date needTime;//占用日期
    private String weekDay;//星期
    private String time;//时间
    private int timeId;
    private int classroomId;
    private String things;
}
