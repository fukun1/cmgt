package com.kk.cmgt.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BookingStatusVo {
    private int bookingId;
    private String classroom;//教学楼+教室号
    private Date useTime;//使用日期
    private String classroomType;
    private String bookingContent;
    private int bookingWeek;
    private String bookingWeekDay;
    private String bookTime;
    private String className;
    private Date updateTime;
}
