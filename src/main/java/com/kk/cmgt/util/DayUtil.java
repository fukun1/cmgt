package com.kk.cmgt.util;

import com.kk.cmgt.pojo.StartTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DayUtil {
    //根据周,星期,学期开始日期计算出日期
    public Date getDate(int week,String weekDay,Date startDay){
        String s[]={"星期一","星期二","星期三","星期四","星期五","星期六","星期日"};
        Date Day = new Date();
        int weekday = 0;
        for(int i=0;i<s.length;i++){
            if(weekDay.equals(s[i])){
                weekday=i;
            }
        }
        int days=(week-1)*7+weekday;
        Calendar calendar  =   Calendar.getInstance();
        calendar.setTime(startDay); //需要将date数据转移到Calender对象中操作
        calendar.add(calendar.DATE, days);//把日期往后增加n天.正数往后推,负数往前移动
        Day=calendar.getTime();   //这个时间就是日期往后推一天的结果
        return Day;
    }
//    public static void main(String[] args) {
//        DayUtil dayUtil = new DayUtil();
//        String time = "2020-8-31 07:11:24";
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        try {
//            Date date = sdf.parse(time);
//            System.out.println(dayUtil.getDate(1,"星期三",date));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }

    public Date addDate(int n,Date date){
        Date day = new Date();
        Calendar calendar  =   Calendar.getInstance();
        calendar.setTime(date); //需要将date数据转移到Calender对象中操作
        calendar.add(calendar.DATE, n);//把日期往后增加n天.正数往后推,负数往前移动
        day=calendar.getTime();   //这个时间就是日期往后推一天的结果
        return day;
    }
}
