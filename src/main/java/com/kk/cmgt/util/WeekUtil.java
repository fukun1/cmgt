package com.kk.cmgt.util;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class WeekUtil {

    //判断当前周是否存在当前周,判断本周次是否存在
    //传入值周段，判断周
    public boolean weekPD(String weeks,int week){
        boolean flag=false;
        String[] array=weeks.split("-");
        for(int i=Integer.valueOf(array[0]);i<Integer.valueOf(array[1])+1;i++){
            if(week==i){
                flag=true;
            }
        }
        return flag;
    }

//    public static void main(String[] args) {
//        WeekUtil weekUtil = new WeekUtil();
//        System.out.println(weekUtil.weekPD("1-18",19));
//    }

    public String getWeekOfDate(Date date){
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        //一周的第几天
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    public String getWeekByInt(int i){
        String[] weekDays = {null,"星期一", "星期二", "星期三", "星期四", "星期五", "星期六","星期日"};
        return weekDays[i];
    }

    public int getWeekByString(String weekDay){
        String[] weekDays = {null,"星期一", "星期二", "星期三", "星期四", "星期五", "星期六","星期日"};
        int week=0;
        for(int i=1;i<weekDays.length;i++){
            if(weekDays[i].equals(weekDay)){
                week=i;
                break;
            }
        }
        return week;
    }

}
