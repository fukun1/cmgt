package com.kk.cmgt.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    //计算当前日期是第几周
    public int getWeek(Date date,Date startDate){
        int week = 0;
        int day=daysBetween(startDate,date);
        week=day/7+1;
        return week;
    }

    public static int daysBetween(Date smdate,Date bdate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(smdate);
        long time1 = cal.getTimeInMillis();
        cal.setTime(bdate);
        long time2 = cal.getTimeInMillis();
        long between_days=(time2-time1)/(1000*3600*24);
        return Integer.parseInt(String.valueOf(between_days));
    }

//    public static void main(String[] args) {
//        DateUtil dateUtil = new DateUtil();
//        String time = "2020-8-31 07:11:24";
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        try {
//            Date date = sdf.parse(time);
//            int week=dateUtil.getWeek(date);
//            System.out.println(week);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }
    
}
