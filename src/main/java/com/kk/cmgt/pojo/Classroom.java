package com.kk.cmgt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Classroom {
    private int classroomId;
    private int classroomFloor;//楼层
    private int classroomCapacity;
    private String classroomNumbering;//教室号
    private String classroomType;
    private Date classroomCreateTime;
    private Date classroomUpdateTime;
    private String classroomName;
}
