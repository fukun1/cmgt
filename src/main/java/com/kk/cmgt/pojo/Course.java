package com.kk.cmgt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Course {

    private int courseID;
    private String courseName;
    private String courseTeacher;
    private String courseClass;
    private String courseWeek;//在哪一个周上课
    private String courseDay;//在这周哪一天
    private int courseTimer;//在这一天的那个时候
    private int status;//状态 0为单双 1单 2双
    private int classRoomID;
    private Date courseCreateTime;
    private Date courseUpdateTime;


}
