package com.kk.cmgt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Booking {
    private int bookingId;
    private int classroomId;
    private int classId;
    private String bookingContent;//备注
    private int bookingWeek;//第几周
    private int bookingWeekDay;//星期几
    private int bookingClass;//上课时间
    private Date bookingCreateTime;
    private Date bookingUpdateTime;

}
