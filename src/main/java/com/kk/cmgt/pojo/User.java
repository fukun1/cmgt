package com.kk.cmgt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private int userID;
    private int classID;
    private int userStudentID;
    private String userPassword;
    private String userName;
    private int userGender;
    private String userNation;
    private Date userBirthday;
    private Date userCreateTime;
    private Date userUpdateTime;
    private int userStatus;

}

