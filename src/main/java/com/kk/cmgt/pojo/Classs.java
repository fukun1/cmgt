package com.kk.cmgt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.TypeAlias;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TypeAlias("class")
public class Classs {
    private int classId;
    private String className;
    private int classPeopleNumber;
    private String classDepartment;//系别
    private String classGrade;//年级
    private String classProfession;//专业
    private Date classCreateTime;
    private Date classUpdateTime;
}
