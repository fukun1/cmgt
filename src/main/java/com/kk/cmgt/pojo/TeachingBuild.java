package com.kk.cmgt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeachingBuild {
    private int teachingBuildID;
    private String teachingBuildName;//名称
    private int teachingBuildFloors;//楼层
    private Date teachingBuildCreateTime;
    private Date teachingBuildUpdateTime;
}


