package com.kk.cmgt.controller.admin;

import com.kk.cmgt.mapper.UserMapper;
import com.kk.cmgt.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/admin/user")
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @GetMapping("")
    public String showUsers(Model model){
        List<User> userList = userMapper.selectAllUser();
        List<User> superUserList =new ArrayList<>();
        for(User user : userList){
            if(user.getUserID()<10)superUserList.add(user);
        }
        model.addAttribute("userList",superUserList);
        return "admin/User/index";
    }

    @GetMapping("/add")
    public String addUser(){
        return "admin/User/add";
    }

    @PostMapping("/addUser")
    public String addUserPost(User user){
        user.setUserCreateTime(new Date());
        //user.setClassR
        int status = userMapper.insertUser(user);
        return "redirect:/admin/user";
    }


    @GetMapping("/edit")
    public String editUser(@RequestParam("id") int id, Model model){
        User user = userMapper.selectUserById(id);
        model.addAttribute("user",user);
        return "admin/User/edit";
    }

    @PostMapping("/editPost")
    public String editUserPost(User user){
        user.setUserUpdateTime(new Date());
        System.out.println("==========="+user.getUserName());
        int status = userMapper.updateUser(user);
        return "redirect:/admin/user";
    }

    @PostMapping("/deleteUser")
    public String deleteUser(@RequestParam("id") int id) {
        userMapper.deleteUser(id);
        return "redirect:/admin/user";
    }

}
