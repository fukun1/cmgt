package com.kk.cmgt.controller.admin;

import com.kk.cmgt.mapper.BookingMapper;
import com.kk.cmgt.mapper.ClassroomMapper;
import com.kk.cmgt.mapper.UserMapper;
import com.kk.cmgt.pojo.Booking;
import com.kk.cmgt.pojo.Classroom;
import com.kk.cmgt.pojo.User;
import com.kk.cmgt.vo.BookingAdminVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.QSort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/admin/booking")
public class BookingController {

    @Autowired
    private BookingMapper bookingMapper;

    @Autowired
    private ClassroomMapper classroomMapper;

    /***
     * 查看空教室申请
     * @param model
     * @return
     */
    @RequestMapping("")
    public String showUsers(Model model){
        List<Booking>bookingList = bookingMapper.selectAllBookings();
        List<BookingAdminVo> bookingAdminVoList=new ArrayList<>();

        for(Booking booking : bookingList){
            BookingAdminVo bookingAdminVo =new BookingAdminVo();
            Classroom classRoom=classroomMapper
                    .selectClassroomById(booking.getClassroomId());
            BeanUtils.copyProperties(booking,bookingAdminVo);
            bookingAdminVo.setClassRoomName(classRoom.getClassroomName());
            bookingAdminVo.setClassroomCapacity(classRoom.getClassroomCapacity());
            bookingAdminVoList.add(bookingAdminVo);
        }
        model.addAttribute("bookingList",bookingAdminVoList);
        return "admin/Booking/index";
    }

    /***
     * 批准申请空教室
     * @param id
     * @param booking
     * @return
     */
    @GetMapping("/editAllow")
    public String editUserPost(@RequestParam("id") int id, Booking booking){
        booking.setBookingUpdateTime(new Date());
        booking.setBookingId(id);
        int status = bookingMapper.updateBookingUpdateTime(booking);
        return "redirect:/admin/booking";
    }
}
