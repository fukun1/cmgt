package com.kk.cmgt.controller.user;

import com.kk.cmgt.dto.JsonResult;
import com.kk.cmgt.mapper.UserMapper;
import com.kk.cmgt.pojo.User;
import com.kk.cmgt.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;

@Controller
@RequestMapping("user/information")
public class UserInformationController {

    @Autowired
    UserMapper userMapper;

    @GetMapping("")
    public String index(Model model, HttpSession  session){
        User user= (User) session.getAttribute("user_session");
        model.addAttribute("user",user);
        return "user/User/index";
    }

    @GetMapping("/updateInformation")
    public String information(Model model,HttpSession session){
        User user= (User) session.getAttribute("user_session");
        model.addAttribute("user",user);
        return "user/User/information";
    }
    @PostMapping("/updateInformation")
    @ResponseBody
    public JsonResult updateInformation(@RequestParam(value ="userName") String userName,
                               @RequestParam(value ="userStudentID") int userStudentID,
                               @RequestParam(value ="userGender") int userGender,
                               @RequestParam(value ="userNation") String userNation,
                               HttpSession session) {
        User user1 = (User) session.getAttribute("user_session");
        User user = new User();
        Date date=new Date();
        user.setUserID(user1.getUserID());
        user.setUserName(userName);
        user.setUserStudentID(userStudentID);
        user.setUserGender(userGender);
        user.setUserNation(userNation);
        user.setUserUpdateTime(date);
        int i =userMapper.updateUserInformation(user);
        if(i==1){
            session.removeAttribute("user_session");
            return new JsonResult(true, "修改成功");
        }else {
            return new JsonResult(false, "修改失败");
        }
    }

    @GetMapping("/updatePassword")
    public String password(){
        return "user/User/password";
    }

    @PostMapping("/updatePassword")
    @ResponseBody
    public JsonResult updatePassword(@RequestParam(value ="newPassword") String newPassword,
                                        @RequestParam(value ="oldPassword") String oldPassword,
                                        HttpSession session) {
        User user1 = (User) session.getAttribute("user_session");
        User user = new User();
        user.setUserID(user1.getUserID());
        String md5NewPassword = MD5Util.getMD5(newPassword);
        user.setUserPassword(md5NewPassword);
        String md5OldPassword = MD5Util.getMD5(oldPassword);
        if(md5OldPassword.equals(user.getUserPassword())){
            return new JsonResult(false, "原密码错误！");
        }else {
            int i =userMapper.updateUserPassword(user);
            if(i==1){
                session.removeAttribute("user_session");
                return new JsonResult(true, "密码修改成功");
            }else {
                return new JsonResult(false, "密码修改失败");
            }
        }
    }
}
