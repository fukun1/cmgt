package com.kk.cmgt.controller.user;

import com.kk.cmgt.dto.JsonResult;
import com.kk.cmgt.mapper.ClasssMapper;
import com.kk.cmgt.mapper.UserMapper;
import com.kk.cmgt.pojo.Classs;
import com.kk.cmgt.pojo.User;
import com.kk.cmgt.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("")
public class LoginController {

    @Autowired
    UserMapper userMapper;
    @Autowired
    ClasssMapper classsMapper;


    @GetMapping(value = {"/login",""})
    public String login(HttpSession session, Model model){
        User user = (User) session.getAttribute("user_session");
        if (user != null) {
            if(user.getUserStatus()==0){
                //return "redirect:/admin/index";
                return "admin/index";
            }else {
                return "user/index";
            }
        }
        List<Classs> list = classsMapper.selectAllClass();
        model.addAttribute("list",list);
        return "login";
    }

    @PostMapping("/getLogin")
    @ResponseBody
    public JsonResult getLogin(@RequestParam(value ="name") String name,
                               @RequestParam(value ="password") String password, HttpSession session) {
        User user = userMapper.selectUserByName(name);
        if(user!=null){
            if(user.getUserPassword().equals(MD5Util.getMD5(password))){
                session.setAttribute("user_session", user);
                return new JsonResult(true, "登录成功");
            }else {
                return new JsonResult(false, "密码错误");
            }
        }else {
            return new JsonResult(false, "用户名错误");
        }
    }

    @GetMapping("/LoginOut")
    public String loginOut(HttpSession session){
        session.removeAttribute("user_session");
        return "login";
    }

    @PostMapping("/updatePassword")
    @ResponseBody
    public JsonResult updatePassword(@RequestParam(value ="userStudentID") int userStudentID,
                                     @RequestParam(value ="userPassword") String userPassword,
                                     @RequestParam(value ="userName") String userName){
        User user=userMapper.selectUserByName(userName);
        User user1=new User();
        if(user!=null){
            if(userStudentID==user.getUserStudentID()){
                user1.setUserID(user.getUserID());
                userPassword=MD5Util.getMD5(userPassword);
                user1.setUserPassword(userPassword);
                int i=userMapper.updateUserPassword(user1);
                if(i==1){
                    return new JsonResult(true, "密码更新成功！");
                }else {
                    return new JsonResult(false, "密码更新失败！");
                }
            }else {
                return new JsonResult(false, "用户名与学号不对应！");
            }
        }else {
            return new JsonResult(false, "用户名不存在！");
        }
    }
    @PostMapping("register")
    @ResponseBody   //classID;userStudentID;userPassword;userName;userGender;userNation;
    public JsonResult register(User user){
        User user1=new User();
        user1.setClassID(user.getClassID());
        user1.setUserPassword(MD5Util.getMD5(user.getUserPassword()));
        user1.setUserStudentID(user.getUserStudentID());
        user1.setUserName(user.getUserName());
        user1.setUserGender(user.getUserGender());
        user1.setUserNation(user.getUserNation());
        user1.setUserStatus(1);
        user1.setUserCreateTime(new Date());
        User user2 = userMapper.selectUserByName(user.getUserName());
        User user3 = userMapper.selectUserByStudentId(user.getUserStudentID());
        if(user2!=null){
            return new JsonResult(false, "用户名已存在！");
        }
        if(user3!=null){
            return new JsonResult(false, "学号已经注册！");
        }
        int i=userMapper.insertUser(user1);
        if(i==1){
            return new JsonResult(true, "注册成功！");
        }else {
            return new JsonResult(false, "注册失败！");
        }


    }

}
