package com.kk.cmgt.controller.user;

import com.kk.cmgt.dto.JsonResult;
import com.kk.cmgt.mapper.*;
import com.kk.cmgt.pojo.*;
import com.kk.cmgt.util.DayUtil;
import com.kk.cmgt.util.MD5Util;
import com.kk.cmgt.util.WeekUtil;
import com.kk.cmgt.vo.BookingStatusVo;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/user/booking")
public class UserBookingController {
    @Autowired
    BookingMapper bookingMapper;
    @Autowired
    ClassroomMapper classroomMapper;
    @Autowired
    TeachingBuildMapper teachingBuildMapper;
    @Autowired
    TimerMapper timerMapper;
    @Autowired
    StartTimeMapper startTimeMapper;
    @Autowired
    ClasssMapper classsMapper;

    @GetMapping("")
    public String index(Model model, HttpSession session){
        User user = (User) session.getAttribute("user_session");
        List<BookingStatusVo> bookingStatusVos = new ArrayList<>();
        List<Booking> bookings = bookingMapper.selectBookingByClass(user.getClassID());
        StartTime startTime = startTimeMapper.selectStartTimeById(1);
        Classs classs=classsMapper.selectClassById(user.getClassID());
        for(Booking booking:bookings){
            BookingStatusVo bookingStatusVo=new BookingStatusVo();
            bookingStatusVo.setBookingId(booking.getBookingId());
            Classroom classroom = classroomMapper.selectClassroomById(booking.getClassroomId());
            TeachingBuild teachingBuild =teachingBuildMapper.selectTeachingBuildById(classroom.getClassroomCapacity());
            bookingStatusVo.setClassroom(teachingBuild.getTeachingBuildName()+classroom.getClassroomName());
            bookingStatusVo.setClassroomType(classroom.getClassroomType());
            bookingStatusVo.setBookingContent(booking.getBookingContent());
            bookingStatusVo.setBookingWeek(booking.getBookingWeek());
            WeekUtil weekUtil=new WeekUtil();
            bookingStatusVo.setBookingWeekDay(weekUtil.getWeekByInt(booking.getBookingWeekDay()));
            Timer timer = timerMapper.selectTimerById(booking.getBookingClass());
            bookingStatusVo.setBookTime(timer.getTimerTime());
            int week=(booking.getBookingWeek()-1)*7+booking.getBookingWeekDay();
            DayUtil dayUtil=new DayUtil();
            bookingStatusVo.setUseTime(dayUtil.addDate(week,startTime.getStartTime()));
            bookingStatusVo.setClassName(classs.getClassName());
            if(booking.getBookingUpdateTime()!=null){
                bookingStatusVo.setUpdateTime(booking.getBookingUpdateTime());
            }
            bookingStatusVos.add(bookingStatusVo);
        }
        model.addAttribute("bookingStatusVos",bookingStatusVos);
        return "user/Booking/index";
    }

    @PostMapping("/delete")
    @ResponseBody
    public JsonResult delete(@RequestParam(value ="bookingId") int bookingId) {
        int i=bookingMapper.deleteBooking(bookingId);
        if(i==1){
            return new JsonResult(true, "取消预约成功！");
        }else {
            return new JsonResult(false, "取消预约失败！");
        }
    }
}
