package com.kk.cmgt.controller.user;

import com.kk.cmgt.dto.JsonResult;
import com.kk.cmgt.mapper.*;
import com.kk.cmgt.pojo.*;
import com.kk.cmgt.service.UserClassroomService;
import com.kk.cmgt.util.DateUtil;
import com.kk.cmgt.util.DayUtil;
import com.kk.cmgt.util.WeekUtil;
import com.kk.cmgt.vo.BookingVo;
import com.kk.cmgt.vo.CourseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/user/classroom")
public class UserClassroomController {
    @Autowired
    StartTimeMapper startTimeMapper;
    @Autowired
    UserClassroomService userClassroomService;
    @Autowired
    ClassroomMapper classroomMapper;
    @Autowired
    TimerMapper timerMapper;
    @Autowired
    TeachingBuildMapper teachingBuildMapper;
    @Autowired
    BookingMapper bookingMapper;
    //教室从今天开始到后面7天的占用情况,找这一周后几天的数据，与下一周前几天的数据
    //教室从今天开始到后面7天内的空教室情况
    @GetMapping("")
    public String classroom(Model model){
        Date date =new Date();
        List<CourseVo> courses=userClassroomService.getCourse(date);
        model.addAttribute("courses",courses);
        return "user/Node/index";
    }
    @PostMapping("")
    public String classroomByName(Model model,String building){
        StartTime startTime = startTimeMapper.selectStartTimeById(1);
        List<CourseVo> courses=new ArrayList<>();
        List<CourseVo> courseVos=userClassroomService.getCourse(startTime.getStartTime());
        for(CourseVo courseVo:courseVos){
            if(courseVo.getTeachingBuild().equals(building)){
                courses.add(courseVo);
            }
        }
        if(building.equals("") || building==null){
            courses=courseVos;
        }
        model.addAttribute("courses",courses);
        return "user/Node/index";
    }

    @GetMapping("/booking")
    public String noBooking(Model model){
        Date date = new Date();
        List<BookingVo> bookingVos=userClassroomService.getNotBooking(date);
        model.addAttribute("bookingVos",bookingVos);
        return "user/Classroom/index";
    }

    @PostMapping("/booking")
    public String noBookingByName(Model model,String roomBuilding,String classroom){
        Date date = new Date();
        List<BookingVo> bookingVos=userClassroomService.getNotBooking(date);
        if(roomBuilding.equals("") || roomBuilding==null){
            if(classroom.equals("") || classroom==null){
                model.addAttribute("bookingVos",bookingVos);
            }else {
                List<BookingVo> bookingVos1 = new ArrayList<>();
                for(BookingVo bookingVo:bookingVos){
                    if(bookingVo.getClassroom().equals(classroom)){
                        bookingVos1.add(bookingVo);
                    }
                }
                model.addAttribute("bookingVos",bookingVos1);
            }
        }else {
            List<BookingVo> bookingVos2 = new ArrayList<>();
            if(classroom.equals("") || classroom==null){
                for(BookingVo bookingVo:bookingVos){
                    if(bookingVo.getTeachingBuild().equals(roomBuilding)){
                        bookingVos2.add(bookingVo);
                    }
                }
            }else {
                for(BookingVo bookingVo:bookingVos){
                    if(bookingVo.getClassroom().equals(classroom) && bookingVo.getTeachingBuild().equals(roomBuilding)){
                        bookingVos2.add(bookingVo);
                    }
                }
            }
            model.addAttribute("bookingVos",bookingVos2);
        }
        return "user/Classroom/index";
    }


    @GetMapping("/addBooking")
    public String addBooking(BookingVo bookingVo,Model model){
        BookingVo bookingVo1 = new BookingVo();
        Classroom classroom = classroomMapper.selectClassroomById(bookingVo.getClassroomId());
        TeachingBuild teachingBuild = teachingBuildMapper.selectTeachingBuildById(classroom.getClassroomCapacity());
        Timer timer = timerMapper.selectTimerById(bookingVo.getTimeId());
        bookingVo1.setClassroom(classroom.getClassroomName());
        bookingVo1.setClassroomType(classroom.getClassroomType());
        bookingVo1.setTeachingBuild(teachingBuild.getTeachingBuildName());
        bookingVo1.setTime(timer.getTimerTime());
        DayUtil dayUtil = new DayUtil();
        bookingVo1.setNeedTime(dayUtil.addDate(-1,bookingVo.getNeedTime()));
        bookingVo1.setTimeId(bookingVo.getTimeId());
        bookingVo1.setClassroomId(bookingVo.getClassroomId());
        //教学楼教室号用途日期时间
        model.addAttribute("bookingVo",bookingVo1);
        return "user/Classroom/add";
    }

    @PostMapping("/addBooking")
    @ResponseBody
    public JsonResult saveBooking(BookingVo bookingVo, HttpSession session){
        User user = (User) session.getAttribute("user_session");
        Booking booking=new Booking();
        booking.setClassroomId(bookingVo.getClassroomId());
        booking.setBookingContent(bookingVo.getThings());
        booking.setBookingClass(bookingVo.getTimeId());
        StartTime startTime=startTimeMapper.selectStartTimeById(1);
        DateUtil dateUtil = new DateUtil();
        int week = dateUtil.getWeek(bookingVo.getNeedTime(),startTime.getStartTime());//第几周
        booking.setBookingWeek(week);
        WeekUtil weekUtil = new WeekUtil();
        String weekDay=weekUtil.getWeekOfDate(bookingVo.getNeedTime());
        booking.setBookingWeekDay(weekUtil.getWeekByString(weekDay));
        Date date = new Date();
        booking.setBookingCreateTime(date);
        booking.setClassId(user.getClassID());
        int i=bookingMapper.insertBooking(booking);
        if(i==1){
            return new JsonResult(true,"预约成功");
        }else {
            return new JsonResult(false,"预约失败");
        }

    }


}
