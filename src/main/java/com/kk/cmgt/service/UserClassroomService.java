package com.kk.cmgt.service;

import com.kk.cmgt.pojo.Course;
import com.kk.cmgt.vo.BookingVo;
import com.kk.cmgt.vo.CourseVo;

import java.util.Date;
import java.util.List;

public interface UserClassroomService {

    List<CourseVo> getCourse(Date startTime);//获取往后一周内的所以教室占用情况

    List<BookingVo> getNotBooking(Date startTime);//获取往后一周没有被占用的教室情况



}
