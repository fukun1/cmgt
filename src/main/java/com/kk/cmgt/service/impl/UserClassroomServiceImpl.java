package com.kk.cmgt.service.impl;

import com.kk.cmgt.mapper.*;
import com.kk.cmgt.pojo.*;
import com.kk.cmgt.service.UserClassroomService;
import com.kk.cmgt.util.DateUtil;
import com.kk.cmgt.util.DayUtil;
import com.kk.cmgt.util.WeekUtil;
import com.kk.cmgt.vo.BookingVo;
import com.kk.cmgt.vo.CourseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserClassroomServiceImpl implements UserClassroomService {

    @Autowired
    CourseMapper courseMapper;
    @Autowired
    ClassroomMapper classroomMapper;
    @Autowired
    TeachingBuildMapper teachingBuildMapper;
    @Autowired
    BookingMapper bookingMapper;
    @Autowired
    TimerMapper timerMapper;
    @Autowired
    StartTimeMapper startTimeMapper;

    @Override
    public List<CourseVo> getCourse(Date startTime){
          List<CourseVo> courses1 = new ArrayList<>();
          List<Course> courses2 = courseMapper.selectAllCourse();
          List<Booking> bookings=bookingMapper.selectAllBookings();
          StartTime startTime1 = startTimeMapper.selectStartTimeById(1);
          DayUtil dayUtil = new DayUtil();
          DateUtil dateUtil = new DateUtil();//计算周数
          WeekUtil weekUtil=new WeekUtil();
          for(int i=1;i<5;i++){
              Date date = dayUtil.addDate(i,startTime);
              int week = dateUtil.getWeek(date,startTime1.getStartTime());//计算周数
              //System.out.println(week);
              String weekDay = weekUtil.getWeekOfDate(date);//计算星期
              for(Course course:courses2){
                  if(course.getStatus()==0 || (course.getStatus()==1 && week%2!=0) || (course.getStatus()==2 && week%2==0)){
                      if(weekUtil.weekPD(course.getCourseWeek(),week)){
                          if(course.getCourseDay().equals(weekDay)){
                              Classroom classroom=classroomMapper.selectClassroomById(course.getClassRoomID());
                              TeachingBuild teachingBuild=teachingBuildMapper.selectTeachingBuildById(classroom.getClassroomCapacity());
                              Timer timer=timerMapper.selectTimerById(course.getCourseTimer());
                              CourseVo vo = new CourseVo(teachingBuild.getTeachingBuildName(),classroom.getClassroomName(),classroom.getClassroomType(),"课程:"+course.getCourseName(),0,date,weekDay,timer.getTimerTime());
                              courses1.add(vo);
                          }
                      }
                  }
              }
              for(Booking booking:bookings){
                 if(week==booking.getBookingWeek()){
                     String weekName=weekUtil.getWeekByInt(booking.getBookingWeekDay());
                     if(weekName.equals(weekDay)){
                         Classroom classroom=classroomMapper.selectClassroomById(booking.getClassroomId());
                         TeachingBuild teachingBuild=teachingBuildMapper.selectTeachingBuildById(classroom.getClassroomCapacity());
                         Timer timer=timerMapper.selectTimerById(booking.getBookingClass());
                         CourseVo vo = new CourseVo(teachingBuild.getTeachingBuildName(),classroom.getClassroomName(),classroom.getClassroomType(),"占用："+booking.getBookingContent(),1,date,weekDay,timer.getTimerTime());
                         courses1.add(vo);
                     }
                 }
              }
          }
          return courses1;
      }

    @Override
    public List<BookingVo> getNotBooking(Date startTime) {//没有占用及除去占用时间就是没有占用的时间
        List<BookingVo> bookingVos = new ArrayList<>();//用于存放数据
        List<CourseVo> courseVos = getCourse(startTime);//获取后七天的教室占用情况
        List<Classroom> classrooms=classroomMapper.selectAllClassroom();//获取所以教室
        StartTime startTime1 = startTimeMapper.selectStartTimeById(1);
        DayUtil dayUtil = new DayUtil();
        DateUtil dateUtil = new DateUtil();//计算周数
        WeekUtil weekUtil=new WeekUtil();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for(int i=1;i<5;i++){
            Date date = dayUtil.addDate(i,startTime);
            int week = dateUtil.getWeek(date,startTime1.getStartTime());//计算周数
            //System.out.println(week);
            String weekDay = weekUtil.getWeekOfDate(date);//计算星期
            for(Classroom classroom:classrooms){
                TeachingBuild teachingBuild = teachingBuildMapper.selectTeachingBuildById(classroom.getClassroomCapacity());
                for(int m=1;m<6;m++){
                    Timer timer=timerMapper.selectTimerById(m);
                    boolean flag=true;
                    for(CourseVo courseVo:courseVos){
                        if(courseVo.getTeachingBuild().equals(teachingBuild.getTeachingBuildName()) &&
                        courseVo.getClassroom().equals(classroom.getClassroomName()) &&
                        courseVo.getTime().equals(timer.getTimerTime()) &&
                        sdf.format(courseVo.getNeedTime()).equals(sdf.format(date))){
                            flag=false;
                            break;
                        }
                    }
                    if(flag){
                        bookingVos.add(new BookingVo(teachingBuild.getTeachingBuildName(),classroom.getClassroomName(),classroom.getClassroomType(),
                                date,weekDay,timer.getTimerTime(),timer.getTimerId(),classroom.getClassroomId(),null));
                    }
                }
            }
        }
        return bookingVos;
    }



}
