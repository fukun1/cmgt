package com.kk.cmgt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CmgtApplication {

    public static void main(String[] args) {

        SpringApplication.run(CmgtApplication.class, args);
        System.out.println("ready");
    }

}
