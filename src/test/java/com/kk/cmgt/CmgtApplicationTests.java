package com.kk.cmgt;

import com.kk.cmgt.mapper.BookingMapper;
import com.kk.cmgt.mapper.TeachingBuildMapper;
import com.kk.cmgt.mapper.UserMapper;
import com.kk.cmgt.pojo.Booking;
import com.kk.cmgt.pojo.TeachingBuild;
import com.kk.cmgt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.awt.print.Book;
import java.util.Date;
import java.util.List;

@SpringBootTest
class CmgtApplicationTests {

    @Autowired
    BookingMapper bookingMapper;
    @Autowired
    TeachingBuildMapper teachingBuildMapper;
    @Autowired
    UserMapper userMapper;

    @Test
    void contextLoads1() {
        User user = userMapper.selectUserByName("kk");
        System.out.println(user);
    }


    @Test
    void contextLoads() {
        List<Booking> list = bookingMapper.selectAllBookings();
        System.out.println(list);
    }


    @Test
    void insertTeachingBuild(){
        TeachingBuild teachingBuild = new TeachingBuild(1,"10教",8,new Date(),new Date());
        int i = teachingBuildMapper.InsertTeachingBuild(teachingBuild);
        System.out.println(i);
    }

    @Test
    void selectTeachingBuild(){
        List<TeachingBuild> teachingBuilds = teachingBuildMapper.selectAllTeachingBuild();
        for(TeachingBuild teachingBuild:teachingBuilds){
            System.out.println(teachingBuild);
        }
        TeachingBuild teachingBuild2 =teachingBuildMapper.selectTeachingBuildById(00000000001);
        System.out.println(teachingBuild2);
    }


    @Test
    void selectUser(){
        List<User> list = userMapper.selectAllUser();
        System.out.println(list);

    }
}
