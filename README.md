# cmgt

#### 介绍
教室管理系统

#### 技术架构

Springboot 
Lombok
MySQL、Mybatis、Mapper、Pagehelper
Bootstrap 3.3.0
jQuery
alibaba.druid

#### 图片
![输入图片说明](https://images.gitee.com/uploads/images/2021/0318/225558_167b51c4_5502956.jpeg "捕获.JPG")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0318/225610_81480cb8_5502956.jpeg "捕获1.JPG")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0318/225627_01cfd6bf_5502956.jpeg "3.JPG")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0318/225635_a5e5ff13_5502956.jpeg "4.JPG")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0318/225649_5dab7d53_5502956.jpeg "5.JPG")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0318/225658_f8e29d98_5502956.jpeg "6.JPG")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0318/225706_e785ef19_5502956.jpeg "7.JPG")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0318/225716_38363827_5502956.jpeg "8.JPG")